import { Injectable } from '@angular/core';
import { Ibook } from '../Interfaces/ibooks.interface';
import { BooksModel } from '../models/books.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiServiceService } from './api-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksServiceService implements Ibook {
  public books: BooksModel[] = [];
  constructor(private http: HttpClient, private apiService: ApiServiceService) { }

  add(book: BooksModel): Observable<BooksModel> {
    return this.http.post<BooksModel>(this.apiService.urlAPI + "books", book);
  }

  getAll(): Observable<BooksModel[]> {
    return this.http.get<BooksModel[]>(this.apiService.urlAPI + "books");
  }

  getById(id: number): Observable<BooksModel> {
    return this.http.get<BooksModel>(this.apiService.urlAPI + `books/${id}`);
  }
  update(book: BooksModel): Observable<BooksModel> {
    return this.http.put<BooksModel>(this.apiService.urlAPI + "books", book);
  }
  delete(id: number): Observable<string> {
    return this.http.delete<string>(this.apiService.urlAPI + `books/${id}`);
  }


}
