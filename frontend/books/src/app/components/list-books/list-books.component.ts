import { Component, Inject, OnInit } from '@angular/core';
import { Ibook } from 'src/app/Interfaces/ibooks.interface';
import { BooksModel } from 'src/app/models/books.model';
import { BooksServiceService } from 'src/app/services/books-service.service';
import { Router } from '@angular/router';




@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.component.html',
  styleUrls: ['./list-books.component.css']
})
export class ListBooksComponent implements OnInit {

  constructor(@Inject(Ibook) private serviceBook: Ibook, private router: Router) { }

  title: string = "BOOKS";
  booksList: BooksModel[] = [];
  showConfirmDelete: boolean = false;
  bookForDelete: BooksModel = new BooksModel();

  ngOnInit(): void {
    this.getAllBooks();
  }


  getAllBooks() {
    this.serviceBook.getAll().subscribe((data) => this.booksList = data);
  }

  addBook() {
    this.router.navigate(['/newbook']);
  }

  update(id: number) {
    //this.router.navigate(['/newbook']);
    this.router.navigate(['/updatebook', id]);
  }

  delete(book: BooksModel) {
    this.serviceBook.delete(book.id).subscribe((data) => { }, (error: any) => { }, () => { this.getAllBooks(); this.showConfirmDelete = false; });
  }


  ConfirmDelete(book: BooksModel) {
    console.log(book);
    this.bookForDelete = book;
    this.showConfirmDelete = !this.showConfirmDelete;
  }

}
