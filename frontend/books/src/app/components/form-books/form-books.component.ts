import { Component, Inject, Injectable, Input, OnInit } from '@angular/core';
import { BooksModel } from 'src/app/models/books.model';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { Ibook } from 'src/app/Interfaces/ibooks.interface';
import { ElementSchemaRegistry } from '@angular/compiler';


@Component({
  selector: 'app-form-books',
  templateUrl: './form-books.component.html',
  styleUrls: ['./form-books.component.css']
})
export class FormBooksComponent implements OnInit {

  constructor(private router: Router, @Inject(Ibook) private serviceBook: Ibook, private route: ActivatedRoute) { }

  @Input() book: BooksModel = new BooksModel();

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      id != null ? this.serviceBook.getById(parseInt(id)).subscribe((data) => this.book = data,
        (error) => console.log(error)
      ) : '';
    }

  }

  SaveData(form: any) {
    console.log("Enviando datos");
    console.log(form);
    console.log(form.value);
    console.log(this.book);

    if (this.book.id == 0) {
      this.serviceBook.add(this.book).subscribe((data) => this.book = data, (error) => console.log(error), () => { this.Back() });
    }
    else {
      this.serviceBook.update(this.book).subscribe((data) => this.book = data, (error) => console.log(error), () => { this.Back() })
    }

  }

  Back() {
    this.router.navigate(['books']);
  }

}
