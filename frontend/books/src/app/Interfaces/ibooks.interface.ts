import { Observable } from "rxjs";
import { BooksModel } from "../models/books.model";

export abstract class Ibook {

    constructor() { };

    abstract add(book: BooksModel): Observable<BooksModel>;
    abstract getAll(): Observable<BooksModel[]>;
    abstract getById(id: number): Observable<BooksModel>;
    abstract update(book: BooksModel): Observable<BooksModel>;
    abstract delete(id: number): Observable<string>;
}