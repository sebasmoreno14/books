export class BooksModel {
    public id: number = 0;
    public title: string = "";
    public author: string = "";
    public read: boolean = false;
}