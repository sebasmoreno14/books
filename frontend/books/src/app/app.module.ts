import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Ibook } from './Interfaces/ibooks.interface'

import { AppComponent } from './app.component';
import { ListBooksComponent } from './components/list-books/list-books.component';
import { AppRoutingModule } from './app.routing';
import { BooksServiceService } from './services/books-service.service';
import { FormBooksComponent } from './components/form-books/form-books.component';

@NgModule({
  declarations: [
    AppComponent,
    ListBooksComponent,
    FormBooksComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    { provide: Ibook, useClass: BooksServiceService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
