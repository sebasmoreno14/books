import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormBooksComponent } from './components/form-books/form-books.component';
import { ListBooksComponent } from './components/list-books/list-books.component';


const routes: Routes = [
    { path: 'books', component: ListBooksComponent },
    { path: 'newbook', component: FormBooksComponent },
    { path: 'updatebook/:id', component: FormBooksComponent },
    { path: '', redirectTo: 'books', pathMatch: 'full' },
    { path: '**', redirectTo: 'books', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }