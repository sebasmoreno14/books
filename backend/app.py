from flask import Flask, jsonify, request
from flask.globals import session
from flask_sqlalchemy import SQLAlchemy, Model
from werkzeug.exceptions import abort
from flask_marshmallow  import Schema, fields, Marshmallow
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field, fields
import json, decimal, datetime
from sqlalchemy import exc
from flask_cors import CORS


app = Flask(__name__)
cors = CORS(app, resource={
    r"/*":{
        "origins":"*"
    }
})

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://root:root@pg_container:5432/books'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

ma = Marshmallow(app)


class books(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title =  db.Column(db.String(500))
    author =  db.Column(db.String(500))
    read =  db.Column(db.Boolean)


class bookschema(SQLAlchemySchema):
    class Meta:
        model = books
        load_instance=True

    id = auto_field()
    title = auto_field()
    author = auto_field()
    read =  auto_field()

db.create_all()
db.session.commit()

@app.route("/ping", methods=["GET"])
def ping():
    return "API is working..."

@app.route("/books", methods=["GET"])
def getBooks():
    listBooks  = db.session.query(books).all()  # Class 'list'
    book_Schema = bookschema(many=True)
    output = book_Schema.dumps(listBooks)
    return output

@app.route("/books/<int:id>", methods=["GET"])
def getBook(id):
  
    result = db.session.query(books).filter(books.id == id ).first()
    if result:
       book_Schema = bookschema()
       output = book_Schema.dump(result)
       return output
    else:
        abort(404)

@app.route("/books", methods=["POST"])
def newBook():
    book_Schema = bookschema()
    bookObject = books()
    bookObject = book_Schema.load(request.json,session=db.session)
    bookObject.id = None
    db.session.add(bookObject)
    db.session.commit()
    return  book_Schema.dump(bookObject), 201

@app.route("/books", methods=["PUT"] )
def updateBook():
    book_Schema = bookschema()
    bookObject = books()
    bookObject = book_Schema.load(request.json, session=db.session)
    result = db.session.query(books).filter(books.id == bookObject.id ).first()
    db.session.commit()
    return book_Schema.dump(result)

# Devolver 204.
@app.route("/books/<int:id>", methods=["DELETE"])
def deletebook(id):
    result = db.session.query(books).filter(books.id == id).first()
    if result:
        try:
            db.session.delete(result)
            db.session.commit()
            return "", 204
        except exc.IntegrityError:
            return jsonify({"message": "Integrity error"})
        
    else:
        return "", 404


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port='4000')