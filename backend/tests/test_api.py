import os
import tempfile
import pytest
import requests
import json

urlBase = 'http://127.0.0.1:4000/'
existsId = '10'
notExistsId = '2000'

# If return status_code = 200 the api is working
def test_get_ping():
    url = urlBase + 'ping'
    
    resp = requests.get(url)        
    # Validate response headers and body contents, e.g. status code.
    assert resp.status_code == 200
    resp
    # print response full body as text
    print(resp.text)

# If return status_code = 200 its a success test
def test_get_books():
    url = urlBase + 'books'
    
    resp = requests.get(url)        
    # Validate response headers and body contents, e.g. status code.
    assert resp.status_code == 200
    resp
    # print response full body as text
    print(resp.text)

# This test must receive the ID parameter of an existing book
# if status_code = 200 its a success test
def test_get_books_by_id_status_200():
    url = urlBase + 'books/' + existsId
    resp = requests.get(url)        
    # Validate response headers and body contents, e.g. status code.
    assert resp.status_code == 200  
    print(resp.text)

# This test must receive the ID parameter of an book that does not exists,
# if status_code = 404  its a success test
def test_get_books_by_id_status_404():
    url = urlBase + 'books/'+ notExistsId
    
    resp = requests.get(url)        
    # Validate response headers and body contents, e.g. status code.
    assert resp.status_code == 404  
    print(resp.text)

#this test must create a new BOOK, receive the author data in JSON format
# if status_code = 201 its a success test
def test_post_books():
    url = urlBase + 'books'
     # Additional headers.
    headers = {'Content-Type': 'application/json' } 
     # Body
    testbook = {'author': 'Test Author', 'id': 0, 'read': False, 'title': "Test Book"}
     # convert dict to json string by json.dumps() for body data. 
    resp = requests.post(url, headers=headers, data=json.dumps(testbook))  
    # Validate response headers and body contents, e.g. status code.
    assert resp.status_code == 201 
    print(resp.text)

#this test must update the data of and existing BOOK, it receive the author data in JSON format
# if status_code = 200 its a success test
def test_put_books():
    url = urlBase + 'books'
     # Additional headers.
    headers = {'Content-Type': 'application/json' } 
     # Body
    testbook = {'author': 'Update Author', 'id': 8, 'read': True, 'title': "Test Book"}
     # convert dict to json string by json.dumps() for body data. 
    resp = requests.put(url, headers=headers, data=json.dumps(testbook))  
    # Validate response headers and body contents, e.g. status code.
    assert resp.status_code == 200 
    print(resp.text)

#this test must delete an existing book, it receive the ID parameter
# if status_code = 204 its a success test
def test_delete_books():
    url = urlBase + 'books/' + existsId
     # Additional headers.
    headers = {'Content-Type': 'application/json' } 
    resp = requests.delete(url)  
    # Validate response headers and body contents, e.g. status code.
    assert resp.status_code == 204 
    print(resp.text)


