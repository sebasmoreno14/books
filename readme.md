# Practice Test - Books 

This is a web application to management books, the backend is develop in Python and the frontend in angular 9.

## Starting 🚀


### Requeriments 📋

You need to have installed:

* Python 3.8
* Postgresql
* node.js
* Docker
* Docker Compose


### Installation Dev 🔧

For installing frontend dependencies, run the command inside the path books/frontend/books:

    
	npm install
	
For installing backend dependencies, run the command inside the path books/backend:

	pip install -r requeriments.txt
	
Create database:
	
	You need to create and empty database in your postresql server
	
For running the backend as container in docker, run the command inside the path books/backend:

	docker-compose up --build
	
### Configuration 🔧

Configure database URI in app.py file, be ensure that the databasename in your URI match with the database created in the installation stepts:
	
	app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://user:password@server:port/databasename'
	
** example **:

	app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:password@localhost:5432/books'

### Running the project in Dev 🔧

Run the command inside the path books/backend:

	python app.py

Run the command inside the path books/frontend/books:
	
	ng serve

### Endpoints books API 🔧

* **ping** - *To test that the API is running* 
* **books** - *To create, delete, update a book, getAll books or get and specific book* 

## Built in 🛠️

* [Phyton] - Flask (flask_sqlalchemy,flask_marshmallow,marshmallow_sqlalchemy)
* [Angular 9] (Bootstrap, font-awesome)


## Author ✒️

* **Juan Sebastian Moreno Restrepo** 

